# ![GeMMA-logo](images/GeMMA-logo.png) XML Schema Definition #

### Summary ###
This repository is the home of GeMMA.xsd.  GeMMA.xsd is the [XML Schema Definition](https://en.wikipedia.org/wiki/XML_Schema_(W3C)) for GeMMA manifest files, the standardised input for the [GeMMA](http://www.thesoftwarebureau.com/mailmark/why-gemma/) application.

The reason for publishing the xsd is to allow 3rd party developers to create GeMMA compatible output from their software.

### Notes ###
* The xsd defines the .gemma file format that will be accepted by GeMMA.
* It does not cover further data requirements for Royal Mail or UK Mail
    * For example itemid must be unique across the file
    * For UK Mail itemid must be unique to their associated Supply Chain ID for a period of three months
    * Only one product code can be specified within a .gemma file
* Further information can be found on the relevant suppliers web sites (see links below)
* There are 2 samples within this repository:
    * sampleRM.gemma
    * sampleTNT.gemma

### Releases ###
Future releases will be detailed here.  To be notified of future releases, simply create a bitbucket account and 'watch' this repository.

* version 2.2
    * Added new ASOC tags:
        * your-description
        * customer-reference
        * your-reference
        * consignment-reference-number
        * magazine-code
        * issue-id
        * department-id
* version 2.1
    * Added new TNT/Whistl tag:
        * tnt-service-level
* Version 2.0 
    * Added two new TNT/Whistl tags:
        * tnt-service-register
        * tnt-contract-code

### Links ###
* [GeMMA](http://www.thesoftwarebureau.com/mailmark/why-gemma/)
* [The Software Bureau](http://www.thesoftwarebureau.com/)
* Email: support@thesoftwarebureau.com
* [xsd wikipedia definition](https://en.wikipedia.org/wiki/XML_Schema_(W3C))
* [Royal Mail eMHS technical implementation guide](http://www.royalmail.com/mailmark/technical)
* [UK Mail customer information](https://www.ukmail.com/information/customer-information)